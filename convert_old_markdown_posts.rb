require 'bundler/setup'
require 'yaml'
require 'nokogiri'

Dir['source/posts/*/*.md'].sort.each do |file_name|
  file_content = File.read(file_name).split(/^---$/)
  raise if file_content.length != 3
  raise if file_content[0] != ''

  frontmatter = YAML.load(file_content[1])
  raise if (frontmatter.keys - %w(title date tags shortlink headpic headpic_alt headpic_caption excerpt forbidden_to_chinese_viewers attribution)).length > 0

  if frontmatter['headpic_caption'] && frontmatter['headpic_caption'].include?('<')
    frontmatter['attribution'] = frontmatter['headpic_caption']
    frontmatter.delete 'headpic_caption'
  end

  frontmatter['tags'] = frontmatter['tags'].split(' ').map do |tag|
    case tag
    when 'IT'
      'it'
    when '元'
      'meta'
    when '其它'
      'other'
    when '动漫'
      'anime'
    when '合作'
      'collab'
    when '审查'
      'censorship'
    when '心理'
      'psych'
    when '技术'
      'tech'
    when '政治'
      'meter'
    when '教程'
      'tutorial'
    when '游戏'
      'game'
    when '生活'
      'life'
    when '翻译'
      'translation'
    when '胡言乱语'
      'mumbo'
    when '自由软件'
      'free-software'
    when '虚构'
      'fiction'
    when '转载'
      'repost'
    else
      raise tag
    end
  end.join(', ')

  adoc_h = []
  adoc_h << '= ' + frontmatter['title']
  adoc_h << 'FiveYellowMice'
  adoc_h << frontmatter['date'].sub(/(\d{2}):(\d{2})/, '\1h\2m')
  %w(tags shortlink forbidden_to_chinese_viewers headpic headpic_alt headpic_caption attribution excerpt).each do |p|
    adoc_h << ":#{p.gsub('_', '-')}: #{frontmatter[p].to_s.strip}" if frontmatter[p]
  end

  content = file_content[2].strip + "\n"

  content.gsub!(/<small>(.+)<\/small>/, '[small]#\1#')
  content.gsub!(/<big>\*\*(.+)\*\*<\/big>/, '[big]*\1*')
  content.gsub!(/<big>(.+)<\/big>/, '[big]#\1#')
  content.gsub!(/<s>(.+)<\/s>/, '[strike]##\1##')
  content.gsub!(/<ins>(.+)<\/ins>/, '[ins]#\1#')
  content.gsub!(/<del>(.+)<\/del>/, '[strike]##\1##')
  content.gsub!(/<figure>.+?<\/figure>/m) do |html|
    figure = Nokogiri::HTML.fragment(html).children[0]
    img = figure.css('img')[0]
    figcaption = figure.css('figcaption')[0]
    ".#{figcaption.text}\nimage::#{img['src']}[#{img['alt']}]"
  end

  content.gsub!(/(?<!\!)\[([^\[\]]*)\]\(([^\(\)]*)\)/) do |match|
    "link:#{$2}[#{$1}]"
  end

  content.gsub!(/(?<!^)\!\[([^\[\]]*)\]\(([^\(\)]*)\)(?<!$)/) do |match|
    "image:#{$2}[#{$1}]"
  end

  content.gsub!(/^\!\[([^\[\]]*)\]\(([^\(\)]*)\)$/) do |match|
    "image::#{$2}[#{$1}]"
  end

  content.gsub!(/\{\{[^\{\}]*\}\}/) do |match|
    case match
    when '{{ site.baseurl }}'
      '{baseurl}'
    when '{{ site.url }}'
      '{site-url}'
    else
      raise
    end
  end

  content.gsub!(/^(#+) (.+?)(?: \{#(.+)\})?$/) do |match|
    "#{'=' * $1.length} #{$2}" + ($3 ? " [[#{$3}]]" : '')
  end

  content.gsub!(/\{[^\{\}]*\}/) do |match|
    case match
    when '{site-url}', '{baseurl}'
      match
    when '{:rel=nofollow}', '{:rel="nofollow"}'
      ''
    else
      raise match
    end
  end

  content.gsub!(/^-{4,}$/, %('''))
  content.gsub!(/:[\w_-]+:/) do |match|
    {
      ':bow:' => "\u{1f647}",
      ':ghost:' => "\u{1f47b}",
      ':grinning:' => "\u{1f600}",
      ':joy:' => "\u{1f602}",
      ':new_moon_with_face:' => "\u{1f31a}",
      ':smiling_imp:' => "\u{1f608}",
      ':smirk:' => "\u{1f60f}",
      ':sob:' => "\u{1f62d}",
      ':thumbsup:' => "\u{1f44d}",
      ':unamused:' => "\u{1f612}",
      ':wink:' => "\u{1f609}",
    }[match] || match
  end

  content.gsub!(/  $/, ' +')

  content = content.split("\n\n<!--more-->")
  if content.length > 1
    content[0] = content[0].split("\n\n").map{|p| p.start_with?('=') ? p : "[.lead]\n" + p }.join("\n\n")
  end
  content = content.join('')

  new_file_name = file_name.sub(/\.md$/, '.zh.html.adoc').sub(/\d{4}-\d{2}-\d{2}/, frontmatter['date'].split(' ')[0])
  
  puts new_file_name
  File.write(new_file_name, adoc_h.join("\n") + "\n\n" + content)
end

# Check:
# [x] <video>
# [x] \_(:3)<)\_   \( > ∀ <) /
# [x] Fedora callouts
# [x] Taobao image center
# [ ] Footnote
# [x] BBR olist br
# [ ] .lead
