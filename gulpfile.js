const gulp = require('gulp');
const fs = require('fs');
const del = require('del');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

function librariesCopy() {
  let files = {
    'jquery.min.js': 'node_modules/jquery/dist/jquery.min.js',
    'lodash.min.js': 'node_modules/lodash/lodash.min.js',
    'js.cookie.js': 'node_modules/js-cookie/src/js.cookie.js',
    'velocity.min.js': 'node_modules/velocity-animate/velocity.min.js',
    'promise-polyfill.min.js': 'node_modules/promise-polyfill/dist/polyfill.min.js',
  };
  return fs.promises.mkdir('gulp-dest/js/lib', {recursive: true})
  .then(() => Promise.all(Object.entries(files).map((s) => fs.promises.copyFile(s[1], 'gulp-dest/js/lib/' + s[0]))));
}

function cssBuild() {
  return gulp.src(['source/scss/*.scss', '!source/scss/_*.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({}))
    .pipe(cleanCSS())
    .pipe(gulp.dest('gulp-dest/css/'));
}

function cssWatch() {
  return gulp.watch('source/scss/*.scss', {ignoreInitial: false}, cssBuild);
}

function clean() {
  return del('gulp-dest');
}

module.exports = {
  librariesCopy,
  cssBuild,
  cssWatch,
  default: gulp.parallel(librariesCopy, cssBuild),
  watch: gulp.parallel(librariesCopy, cssWatch),
  clean,
};
