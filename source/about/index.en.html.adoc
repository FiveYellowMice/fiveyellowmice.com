= About This Blog
:page-layout: page
:in-sitemap: true

Welcome to FiveYellowMice's personal blog! This page contanins information about this blog, and some FAQs. For an introduction on the person FiveYellowMice, please go to https://fym.moe/[FiveYellowMice's Personal Home Page].

In 2015, inspired by https://program-think.blogspot.com/[Program Think], FiveYellowMice found a way to release their desire to express -- creating this blog. At first the plan was to talk about their personal projects, topics on censorship, and other miscellaneous ideas that requires long pieces text to explain. But as FiveYellowMice realized their ignorance, the number of the former 2 types of articles decreased. Now this blog is predominantly filled by the 3rd type of articles, with occasional preachments on free software and comments on literature works footnote:[Including comics, videos and games]. You can view link:{baseurl}/{lang}/tags/[all the tags] on this blog to overview the type of contents it has at the moment.

== Copyright [[copyright]]

FiveYellowMice believes that freedom is important, and it's also important to maintain existing freedom. Therefore the original contents (i.e. not including works that have been labeled as coming from other authors) on this blog are, unless specifically stated, are published under a https://creativecommons.org/licenses/by-sa/4.0/[CC-BY-SA 4.0] license. In other words, anyone can freely redistribute, modify, or make derivative work on these contents, even commercial uses are allowed, as long as you:

* Include the original author's name.
* Include a link to the oringal webpage.
* If it is modified, make it clear that it is modified.
* And most importantly: also publish under a CC-BY-SA 4.0 license. In other words, also allow others to redistribute, modify and derive your derivative work, under the same conditioins.

TIP: On the bottom of most webpages, you can see link:{baseurl}/images/by-sa.svg[a label indicating CC-BY-SA 4.0].

== Privacy [[privacy]]

This is perhaps not a serious Privacy Policy, but FiveYellowMice still wants to briefly explain what data this website collects.

The blog owner will collect the following information in order to know which of their articles are read in what way:

* URL of the visited page.
* How long the page has been kept open. (*)
* What links are clicked. (*)
* The starting 24 bits of an IPv4 address or the starting 64 bits of an IPv6 address. This will be deleted after 90 days.
* Guesses of the country and ISP based on the IP address.
* Whether the "Do Not Track" setting in the browser has been turned on.
* Past visits from this browser to this website in 90 days (Cookie tracking).

90 days after the visit, the raw data of above items (the records that are made every time a visitor visits) are deleted, and aggregated into relatively anonymized data such as "how many times a given page is visited on a given day". If the "Do Not Track" browser setting is on, items marked with an asterisk are not collected.

[#cookie-consent-description]
A popup box appears when you first visit the website requesting your consent to do cookie tracking. Cookie tracking will not happen until you have consented.

++++
<script>
document.addEventListener('DOMContentLoaded', function() {
  var trackingCookieConsent = parseInt(Cookies.get('trackingCookieConsent') || 0);
  if (trackingCookieConsent !== 0) {
    $('#cookie-consent-description').append([
      ' You have chosen to ' + (trackingCookieConsent > 0 ? 'accept' : 'reject') + ' it, but you can ',
      $('<a aria-role="button" href="javascript:void(0)">').text('revoke this choice').click(function(event) {
        event.preventDefault();
        Cookies.remove('trackingCookieConsent');
        location.reload();
      }),
      '.',
    ]);
  }
});
</script>
++++

If you don't want to leave any tracking records on this website, please uncheck the check box on https://matomo.fiveyellowmice.com/index.php?module=CoreAdminHome&action=optOut&language=en-us&backgroundColor=&fontColor=&fontSize=&fontFamily=[this page].

Additionally, this website uses these third-party services, each subjects under its own (real) Privacy Policy.

* CloudFlare
* Google AdSense
* GitHub
