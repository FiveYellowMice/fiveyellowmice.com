(function(f) {
  if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", f);
  } else {
    f();
  }
})(function () {
  var manuallyShownSpoilers = [];

  $('.spoiler-button').click(function(event) {
    var params = event.target.dataset;
    var spoilerTags = params.spoilerFor.split(',');

    if (params.action === 'show') {
      // Add spoiler tags on the current button to a list of manually shown spoilers.
      spoilerTags.forEach(function(s) {
        if (manuallyShownSpoilers.indexOf(s) === -1) {
          manuallyShownSpoilers.push(s);
        }
      });
      // Show the spoiler if all of its spoiler tags are manually shown, or if it doesn't contain any spoiler tags.
      $('.spoiler').filter(function(i, el) {
        var containsSpoilerFor = [].slice.call(el.classList).filter(function(className) {
          return className.indexOf('spoiler-for-') === 0;
        }).map(function(className) {
          return className.substring(12);
        });
        for (var j = 0; j < containsSpoilerFor.length; j++) {
          if (manuallyShownSpoilers.indexOf(containsSpoilerFor[j]) === -1) {
            return false;
          }
        }
        return true;
      }).addClass('spoiler-shown');

    } else if (params.action === 'hide') {
      spoilerTags.forEach(function(s) {
        // Remove spoiler tags on the current button from a list of manually shown spoilers.
        var indexOfS = manuallyShownSpoilers.indexOf(s);
        if (indexOfS !== -1) {
          manuallyShownSpoilers.splice(indexOfS, 1);
        }
        // Hide all spoilers containing any of the tags
        $('.spoiler-for-'+s).removeClass('spoiler-shown');
      });
      // Hide spoilers with no spoiler tags.
      $('.spoiler').filter(function(i, el) {
        for (var j = 0; j < el.classList.length; j++) {
          if (el.classList[j].indexOf('spoiler-for-') === 0) {
            return false;
          }
        }
        return true;
      }).removeClass('spoiler-shown');
    }

    // Disable spoiler buttons that have the same action, and contain no or the same or a subset of spoiler tags of the currently clicked button, including the currently clicked button itself.
    $('.spoiler-button').filter(function(i, el) {
      if (el.dataset.action !== params.action) {
        return false;
      }
      var testingSpoilerTags = el.dataset.spoilerFor.split(',');
      for (var j = 0; j < testingSpoilerTags.length; j++) {
        if (spoilerTags.indexOf(testingSpoilerTags[j]) === -1) {
          return false;
        }
      }
      return true;
    }).each(function(i, el) {
      el.disabled = true;
      el.innerText = el.dataset.activatedLabel;
    });
  });
});
