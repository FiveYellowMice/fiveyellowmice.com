---
title: 小众的偏好
date: 2018-05-18 18:57
tags: 胡言乱语
shortlink: vzf
---

我最近有一种感觉，我们喜欢一样东西，也许不主只是因为喜欢这样东西本身，而是喜欢这样东西的「小众」属性。就是说，因为一样东西小众，所以我们会更容易喜欢它。我们会更偏好那些看起来「小众」、「独特」的东西，并常常认为那些东西是更高质量的。这种偏好经常被说是「装逼」，但我觉得，这没有什么可以批评的，装装逼也是很正常的事情嘛。

<!--more-->

有个网站，名叫「[少数派](https://sspai.com/){:rel="nofollow"}」，从名字就可以看出来，它就是以「少数」的定位自居的。少数派上面有许许多多的「小众」文章，介绍的就是相对比较「极客」的东西。这边奇奇怪怪的工作效率提升小技巧啦，那边五花八门的各种用途的应用啦，都是以一种「告诉你一个许多人都不知道的东西哦」的感觉，引起大家的兴趣。

尽管喜欢阅读少数派的人可能不同意，不过其实微信公众号上面那些吸睛的标题，许多也是利用了相同的心理。「震惊！百分之九十九的人都不知道的事实」，一看，「哇 99% 的人都不知道，那我看了之后就能成为那 1%了呢」，然后就回去看那篇文章。那至于实际上真的是不是 99% 的人都都不知道，那就肯定不用说了。不过现在，一般人也不会真的去相信「百分之九十九」这样毫无根据的数字了，但即使心知肚明这是瞎说的，在潜意识里面，对这篇文章的好奇心还是增加了。

GNU/Linux 用户群体也是如此。作为 1% 的超边缘用户，在各种角度都倍受歧视。这也是没办法的事情， GNU/Linux 用户这么少，做个支持，成本不值得嘛。但顶着这些困难，还是有许多人坚持了下来。 GNU/Linux 用户们被称作「不切实际」、「天真」，但还是有许多人没有正视这些嘲讽。这其中的原因，或多或少，都有一点「因为这件东西小众」的成分。

购买奢侈品的欲望也是一样。因为它们的价格，拥有奢侈品的人群，当然是小众的了。于是大家就都想跻身进入那个属于富人的行列，甚至不惜贷款，也要让自己爬到那块高耸的价格标签上。

同样的还有动漫文化。起初它们是小众的，人们看到它，觉得「这个东西好棒」便喜欢上了。那时候大家觉得自己对动漫的喜爱，是自己值得珍惜的一个部分。后来这个群体逐渐扩大了，尽管还没有成为主流，但至少已经可以满大街跑，影响到许多主流文化了。这时，便有许多人对这个文化觉得厌烦了，开始反感动漫，并用「二刺塬」这样的话嘲讽。不得忽视的原因，就是这时候反感动漫已经成为了小众。

上面提到的少数派，也是经历了相似的命运。一位独立 iOS 开发者说，他以前曾经很喜欢少数派的风格，经常给它写文章，但是现在「变得大众」之后，就不再喜欢，再也不为它写文章了。

-----

对小众的东西的偏好，是毫无疑问的装逼。但是这样的心理，应该说每个人或多或少都有。我不知道它的心理学名词叫什么，但是要表示与大众不同，古语里面就有不少描述：鹤立鸡群、一枝独秀、红杏出墙——你看，还都是褒义词呢。所以这也不是个值得批评的东西。

实际上，想要追寻小众的东西的心理，与其它心理一样，人类社会长期演化的成果。总体而言，也是有益的。

因为人类会追寻小众，所以就会有富有创造力的东西被创造出来，满足小众的需求。无论是文学、绘画、音乐，还是餐饮、影视、设计。这些小众的东西，使世界充满活力。如果大家都随波逐流，小众的东西都被放弃，那么很快，所有的东西就会变得一成不变了。

因为人类会追寻小众，所以才会有各种观点诞生出来，相互碰撞、变化，在这样的过程中使世界变得更好——不止是政治方面的。如果没有这些小众的观点的话，所有人的心思一致，那就是最可怕的乌托邦了。

所以，请大家一定要好好守护自己所爱的、小众的东西哦。当然，贷款买奢侈品还是算咯。
