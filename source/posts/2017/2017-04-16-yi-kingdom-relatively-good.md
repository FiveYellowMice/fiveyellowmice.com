---
title: 逸国还算不错了：跟美国业界翘楚 Crunchyroll 的对比
date: 2017-04-16 16:23
tags: 动漫 政治
shortlink: xna
headpic: {baseurl}/images/2017/04/bilibili-crunchyroll-mascots.jpg
headpic_alt: 蓝白对橙色
excerpt: |
  Bilibili 是中国数一数二的动漫主题的视频网站，受到许多人的欢迎，但也因为其许多次不光彩的事件以及愚弄粉丝的经历，也受到相当大的争议。创始人叫徐逸，所以也被蔑称作“逸国”。确实， Bilibili 做了许多不光彩的事情，愚弄用户，培养了大量脑残粉，甚至还搞奇奇怪怪的审查，但是从用户体验、从产品的质量来看，他们相对来说还是很不错的。为了证明这一点，我们来做个比较吧，不跟其它的中国公司比，我们来国际化一点，跟美国动漫行业的业界翘楚—— Crunchyroll 比一比。 
---

[Bilibili](http://www.bilibili.com/){:rel=nofollow} 是中国数一数二的动漫主题的视频网站，受到许多人的欢迎，但也因为其许多次不光彩的事件以及愚弄粉丝的经历footnote:[link:https://zh.wikipedia.org/wiki/Bilibili#.E4.BA.8B.E4.BB.B6[维基百科]上记录了几次有争议的事件；link:https://wiki.esu.moe/Bilibili[恶俗维基]上以带有偏见的口吻，记录了更多或更加难以确定真实性的内容。]，也受到相当大的争议。创始人叫徐逸，所以也被蔑称作“逸国”。确实， Bilibili 做了许多不光彩的事情，愚弄用户，培养了大量脑残粉，甚至还搞奇奇怪怪的审查footnote:[除了中国公司无法避免的对政治内容的审查以外， Bilibili 会审查更多奇怪的内容，通常是对他们不利的事件的关键字，比如我见到许多人打不出 Mac 这个词，因为它包含 AC 两个字母。不过很遗憾，我找不到对这件事情的证据，所以你要是不相信的话也是可以理解的。]，但是从用户体验、从产品的质量来看，他们相对来说还是很不错的。为了证明这一点，我们来做个比较吧，不跟其它的中国公司比，我们来国际化一点，跟美国动漫行业的业界翘楚—— Crunchyroll 比一比。

Crunchyroll 是美国的一家在线动画视频串流网站，拥有着播放许多动画的版权，还说能够做到新番在日本的电视上放映一个小时之后，他们就能把字幕做好让大家看。如果你没有见过 Crunchyroll 的话，现在就可以打开[他们的网址](http://www.crunchyroll.com/){:rel=nofollow}看一看。看起来……还不错，对吧？

但是你只要打开一个视频，就可以慢慢地的发现，他们的服务是多么地差劲，他们的体验是多么地糟糕。作为用了有一段时间，还充值了高级用户的人，我觉得我有必要跟大家分享一下， Crunchyroll 有多么差劲。

## 免费用户的体验

![Crunchyroll - Watch Naruto Shippuden, Bleach, Anime Videos and Episodes Free Online]({{ site.baseurl }}/images/2017/04/cr-home-title.png)

看一看它们首页上的标题吧，里面写着： Crunchyroll - Watch Naruto Shippuden, Bleach, Anime Videos and Episodes Free Online 。对，free! 免费观看它上面的所有动画哟！但是别高兴的太早，作为免费用户，你只能看 360p 的画质。360p 也就算了，你还要忍受铺天盖地的广告，一集 24 分钟的动画，开头两分钟广告，中间两分钟广告，放完结束曲、在下集预告之前再放两分钟广告。跟动画本身长度三分之一相当的时间，你就在看广告中度过了。而且他们的广告基本都是在说他们自己，整天就是在一遍一遍地说“快来买我们的高级用户！有高清画质看还没广告！买不了吃亏买不了上当！买之前还可以免费试用两周呢！”还不如放点别的广告，直接赚到广告费呢。

除此之外，作为免费用户，你不能享受号称“日本电视上映一个小时后就可以看”的服务。相反，免费用户要是想看新出的一集动画，得等上一周，也就是基本上下一集已经出了的时候才能看。这个一周的延迟，就是故意设置的，引诱你买高级用户。

## 破烂的 Flash 播放器

![Flash 视频播放器的模样]({{ site.baseurl }}/images/2017/04/cr-player.jpg)

买了高级用户体验就会很好了吗？并没有。作为一个主要业务是在线串流视频的网站，播放器当然是十分重要的，但是 Crunchyroll 的播放器，横看竖看，我都只能给出两个字——破烂——来形容。底下那丑得不属于这个时代的进度条和按钮，感觉就像是十年前用网上随意查找的图标素材鼠标拼凑的一样。那进度条甚至没有在 0 分 0 秒的时候跟最左边对其。右边的四个按钮分别是：自动切换下一集，分出个别的窗口播放，全屏，还有调音量，每个图标的作画风格迥异。其中“自动切换下一集”的按钮跟其它的有一些不同，鼠标悬停上方的时候会显示 autoplay 的提示，但看没有什么问题，但是……全部四个按钮里面，只有这一个按钮有悬停提示，其它的三个都没有，这样的设计有些难以理解。

![鼠标悬停在 autoplay 按钮上会有提示]({{ site.baseurl }}/images/2017/04/cr-player-autoplay-button.png)

对了，我有提到过吗？这个播放器还是 Flash 的，连 Bilibili 都已经有 HTML5 播放器的时候，这位美国业界翘楚依然还在用 Flash ——一个专有、全是安全漏洞、[被遗弃](http://cn.engadget.com/2015/12/02/adobe-flash-professional-now-animate-cc/)、还会让你的笔记本风扇起飞的技术。也就是说，如果你没有装 Flash ，就只能看到一个叫你装 Flash 的提示。如果用移动设备的话，乖乖装客户端去吧。

## 屎一样的移动客户端

图片很难展示一个应用程序的操作体验，所以我就不给主界面截图了。但是你只要用一下，就可以很明显地感觉出，这玩意的设计，跟我画的差不多，这玩意的功能，就只能放个视频，如果你想看看账户设置，或者看一下除了简介与标题以外的动画信息，或者想留个评论打个分，还是得上网页。这个网页还没有为小屏幕设计过。

那既然这个客户端功能如此简陋，只能播放视频，那视频播放器应该要好一些吧？很遗憾，不是的。

![Android 手机上的播放器]({{ site.baseurl }}/images/2017/04/cr-mobile-player.jpg)

言语已经无法形容这个播放器的模样了，如果非要一个字形容的话，就是屎。一到浅色背景就消失的快进和快退按钮还算好的，如果你的机器有虚拟键的话，显示/隐藏控制按钮的体验是这样的：

[subs=+attributes]
++++
<video autoplay loop muted style="width: 100%" onpause="this.play()">
  <source src="{{ site.baseurl }}/images/2017/04/cr-mobile-player-toggle-controls.mp4" type="video/mp4">
  <source src="{{ site.baseurl }}/images/2017/04/cr-mobile-player-toggle-controls.ogv" type="video/ogg">
  <img alt="显示/隐藏控制按钮的体验" src="{{ site.baseurl }}/images/2017/04/cr-mobile-player-toggle-controls.gif">
</video>
++++

要吐了。

你说下载/缓存？做梦。付费用户也做梦。

Android 客户端这么差劲， iOS 的会不会好一点呢？很遗憾，没有什么区别。唯一好一些的，就是 iOS 没有虚拟键，所以不用担心上面提到的恶心效果。

## 土豆服务器

Bilibili 的服务器也不见得很好， 502 Bad Gateway 什么的是日常。但是 Crunchyroll 的服务器可能需要更多的努力。视频放一半卡住是日常，而且这一卡住，八成就没机会自己继续播放了。惟一的办法就是刷新，从头加载一遍最开始的一小段视频，拖一拖进度条。运气不好的时候，看一集动画得要刷新四次左右。

有趣的是，在电脑上，如果视频卡住是不会有“正在加载”的提示或者转圈圈的。也就意味着，如果在静止画面卡住，你可能就不会马上发觉，等过了一会儿，“咦，这静止画面怎么那么长？”

为了照顾他们的土豆服务器，Crunchyroll 最近[做了大量的努力剥削视频的码率](https://medium.com/@Daiz/crunchyrolls-reduced-video-quality-is-deliberate-cost-cutting-at-the-expense-of-paying-customers-c86c6899033b)，结果就是， 1080p 的视频，有时还不如其他地方的 720p 的视频看着清晰，这种区别是微小的，但是对观看体验的影响毋庸置疑，看一下[比较](https://diff.pics/lJV9wcuIEbZf/2)就很明显了。Crunchyroll ，一个用户付费看正版 1080p 动画的地方，搞得还不如许多盗版的画质要高，你说这算什么呢？

## 奇怪的版权保护

在对 Flash 播放器的截图中，你可能已经注意到了：视频顶上有个 "Exclusive stream for FiveYellowMice" 。不难猜出这个是为了保护视频的版权，防止被人录屏传播的手段。但是还是看起来很恶心。

Android 有个功能，可以让应用设定自己能不能被截图，说是保护隐私，保护你妹的隐私！这个功能就是个彻底的错误，除了给用户带来不方便以外没有任何用处。而 [Crunchyroll 的漫画应用](https://play.google.com/store/apps/details?id=com.crunchyroll.crmanga){:rel=nofollow}，将这个错误更加地发扬光大——你不能够在看漫画的时候给自己的设备截图。但是这有个卵用啊？我想翻拍盗版的话，可以拿另一个设备给屏幕拍照，可以 root 了以后绕过这个限制，甚至我直接拿 iOS 设备截图都可以啊？等等，我有毛病，为什么要拿移动设备？直接拿电脑截图不就完了？这个设计没有有效地防范盗版，反而在我想给漫画截一页图，发到网上吐槽一下内容的时候，带来了麻烦。

## 尾声

以上，就是我对 Crunchyroll 的使用感受。尽管为了文章的主题，我有些偏颇，像他们[降低码率后对视频画质的改善](https://medium.com/ellation-tech/improving-video-quality-for-crunchyroll-and-vrv-dd587261a364)就没有在上面提到（现在提到了），Crunchyroll 的一些优势也没有提到。但是我想我的意思大家应该已经明白了：Crunchyroll 的用户体验很差，而 Bilibili ，真的，已经挺不错了。
