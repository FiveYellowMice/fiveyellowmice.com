---
title: 啊我们好屌，美国人都被微信惊呆了！
date: 2016-08-30 03:03
tags: 政治 审查
shortlink: dvm
forbidden_to_chinese_viewers: true
headpic: {baseurl}/images/2016/08/swamp-monster-apps.png
headpic_alt: Swamp monster apps
---

昨天晚上，我的父亲向我发送了一条来自微信公众号的文章链接，许多人应该都有同样的经历——好不容易离开了满是伪科学、自我安慰和[点击诱饵](https://zh.wikipedia.org/wiki/%E6%A0%87%E9%A2%98%E5%85%9A)的微信，却依然要被这样的文章骚扰。为了不让他失望，我还是点开了链接，这一点开不要紧，这霸气的标题就立即就吸住了我的眼睛。它的标题是：

[《纽约时报》5 分钟专门介绍微信，整个美国都被震撼了！！！](https://mp.weixin.qq.com/s?__biz=MjM5NDE1MDYyMQ==&mid=2651012333&idx=2&sn=ec979845d27390e14fc6e410dc5f39ce)

<!--more-->

文章只有几行简短的介绍，然后就是就是纽约时报原本的视频……啊，不对，是纽约时报的视频，经过了字幕制作者的 **处理** ，再上传到腾讯服务器上的视频。你可以点开上面的链接看一下。放心，纽约时报的视频，虽然是被爱国主义者选中的，但还是比较客观的。

视频大概是说，中国的互联网有许多乱七八糟的复制品，但是仍有许多东西值得我们西方学习，比如微信，人们可以用它来完成从宠物护理到聊天约炮到下馆子点菜等几乎所有日常生活需要的事情，而这些事情都不需要离开微信这个应用。整个视频比较客观，语气也十十分平淡，里面并没有如标题所说，将 5 分钟专门用来介绍微信，只是将微信作为例子，表示西方公司还要学习一个，也完全看不出来“整个美国都被震撼了”。但这些都是大家可能已经司空见惯的夺眼球手法了，不够有趣，有趣的事情还在后面。

----------

文章中给出的视频是一个在腾讯视频上的、经过添加字幕等处理过的视频。显然，原本纽约时报发布在 YouTube 上的视频，微信公众号的阅读者是很少愿意去翻墙播放的，而且也看不懂。当然，还有更加重要的原因： **那就是原本的视频不够和谐** 。

如果你能够翻墙，再加上一点微小的思考，就能够很容易地在 YouTube 上找到纽约时报发布的[原版视频](https://www.youtube.com/watch?v=VAesMQ6VtK8)。不得不说，这个视频的播放量确实挺多的，有十万多次，甚至我在 YouTube 的搜索框中输入 "new york times" 的时候， "new york times wechat" 就已经出现在了搜索建议中。

![New York Times WeChat 出现在 YouTube 的搜索建议中]({{ site.baseurl }}/images/2016/08/youtube-search-suggestion.png)

看完了原版视频，才看出修改过的视频是多么滑稽。口说无凭，有图为证，那么首先我来截两张图吧。

抱歉，在放截图之前，我先教大家两个单词（如果你不认识的话）：  
**[internet](https://www.bing.com/dict/search?q=internet)** /'ɪntənet/ n. 互联网，因特网  
**[intranet](https://www.bing.com/dict/search?q=intranet)** /'ɪntrə.net/ n. 局域网，内部网络

这是原版视频 0:28 的时候的截图：  
![原版视频 0:28]({{ site.baseurl }}/images/2016/08/nyt-wechat-0-28-original.png)

这是修改版视频 0:24 秒的时候的截图，对应的是相同的场景：  
![修改版视频 0:24]({{ site.baseurl }}/images/2016/08/nyt-wechat-0-24-edited.png)

哈哈哈哈哈！原版视频没有把台湾画进图中，于是字幕组就往台湾的位置贴上了一个 Doge 头像，好像“此地有银三百两”——装作“这个 Doge 头像底下是台湾”的样子。如此自欺欺人之手法，实在是让在下佩服。而且，不管台湾究竟跟中国大陆是否属于同一个国家，人家都不在局域网中，你说怎么能画进图中呢？

还有这个“因特拉网”是什么鬼？ Intranet 的意思明明是“局域网”，简单明了。可是为什么他们放着现成的、广为人知的词语不用，非要生造一个叫“因特拉网”的词语？怕是不敢将真相说出来吧。

----------

上面的还不是最滑稽的，更加滑稽的是，当你在修改版视频中，从上面截图的位置（也就是说“但是中国的互联网更像是个因特拉网”的时候）播放，下一句话将会是“这就意味着 没有脸谱网 没有推特 没有谷歌”，并配以这三个服务的图标。可是在原版视频中，中间还有一些内容。也就是说，有一些内容被剪掉了。

这些被剪掉的内容有两句话，你可以在原版视频的 0:29 开始看到。如果你听不懂的话，我这边粗略地翻译了一下：  

> It's largly walled off from the western world by it's incredibly complex systems of filters and blocks that we called the Great Fire Wall.  
> 中国的网络被它那非常复杂的过滤与封锁系统与西方世界隔离开来，也就是我们通常叫做 GFW 的东西。
> 
> And basically the Great Fire Wall blocks any foreign site that the Communist Party doesn't think it can control.  
> 简单来说， GFW 封锁了所有共产党认为他们无法控制的外国网站。

这两句话被剪掉的理由已经非常明显了。

被剪掉的内容不止这一处，在原版视频的 4:50 以及修改版视频的 04:33 ，有这样一句话：“它整合了脸谱网 亚马逊 谷歌和 PayPal 的整个数据 一应俱全。”从这句话之后开始，又是一段内容被移除。

> The problem is, all of the data is information Chinese companies are forced to share with the Chinese government, which has a long record of human rights violations. And it isn't exactly shy about stalking the citizens.  
> 问题在于，所有的数据都会属于中国公司被强制分享给中国政府的，这有这很长的人权侵犯记录。而且它（政府）也不害怕跟踪他们的公民。

在结尾，又是半句话被剪。

> Concentrating so much data on so few hands could lay the groundwork of an Orwellian world, where companies and governments can track every single movement you make.  
> 将如此多的数据集中在如此少数人手中，可以为[奥威尔世界](https://zh.wikipedia.org/wiki/%E6%AD%90%E5%A8%81%E7%88%BE%E4%B8%BB%E7%BE%A9)奠定基础，在这样的世界中，公司和政府将可以追踪你的做的每一个动作。

这样的剪辑实在是太过滑稽，让人忍俊不禁。说微信有多方便的时候很兴奋，而一旦谈到 GFW 、隐私、个人权利的时候，就畏畏缩缩不敢面对了。

----------

微信确实很厉害，这也是视频的主题。可是视频中也有一些批评，但这些批评太过尖锐，难以让爱国者们接受，更难以让老大哥接受。于是可怜的字幕组只好选择性失明，将“尖锐的批评”变成了“不痛不痒的批评”。

真的很悲哀，好不容易外国人赞扬一下中国的软件，我们却还是需要过滤里面的内容。我们不敢面对真话，却沉迷于欺骗自己，沉浸在剪辑视频产生的自我陶醉当中。
