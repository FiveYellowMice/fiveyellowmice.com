# frozen_string_literal: true

require 'uri'
require 'nokogiri'

RSpec.describe 'Post sources' do

  it 'have unique slugs and locales' do

    slugs_and_locales = Dir['./source/posts/**/*.html.adoc'].map{|p| p.split('/').last.match(/^\d+-\d+-\d+-(?<slug>.+)\.(?<locale>[a-z]+)\.html\.adoc$/).named_captures }

    slugs_and_locales.each do |a|
      articles_with_same_slug_and_locale = slugs_and_locales.select{|b| a == b }
      if articles_with_same_slug_and_locale.length > 1
        raise "Found #{articles_with_same_slug_and_locale.length} articles having the same slugs and locales #{a[:slug]}.#{a[:locale]}"
      end
    end

  end

end
