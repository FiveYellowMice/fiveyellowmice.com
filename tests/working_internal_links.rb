# frozen_string_literal: true

require 'uri'
require 'nokogiri'

RSpec.describe 'Generated HTML documents' do

  it 'have all internal links working' do

    errors = []

    Dir['./build/**/*.html'].each do |filename|
      next if filename.start_with? './build/bower_components/'

      document = Nokogiri::HTML File.read filename

      document.xpath('//a/@href|//link/@href|//img/@src|//script/@src|//video/@src|//audio/@src|//source/@src|//object/@data|//iframe/@src').each do |attribute|

        next if attribute.text =~ /^(?:[a-z0-9+.-]+\:|\/\/)/ # only internal URL
        link_path = attribute.text.match(/^.*?(?=\?|#|$)/).to_s # strip query and hash
        next if link_path.empty?

        link_path = URI.unescape link_path

        if link_path.start_with? '/' # absolute to webroot
          link_path_absolute = File.expand_path(link_path[1..-1], './build')
        else
          link_path_absolute = File.expand_path(link_path, filename)
        end

        unless File.exist? link_path_absolute
          errors << "#{filename[7..-1]}: #{attribute.parent.to_html}"
        end

      end
    end

    unless errors.empty?
      raise "Found #{errors.length} invalid link#{errors.length == 1 ? '' : 's'}:\n  " + errors.join("\n  ")
    end

  end

end
