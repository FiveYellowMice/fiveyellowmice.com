# frozen_string_literal: true

require 'nokogiri'

RSpec.describe 'Generated HTML documents' do

  it 'have a limited set of external origins' do

    errors = []

    Dir['./build/**/*.html'].each do |filename|
      document = Nokogiri::HTML File.read filename

      document.xpath('//link/@href|//img/@src|//script/@src|//video/@src|//audio/@src|//source/@src|//object/@data|//iframe/@src').each do |attribute|

        next unless attribute.text =~ /^(?:https?\:)?\/\// # only external HTTP(S) URLs
        origin = attribute.text.match(/^(?:https?\:)?\/\/(.*?)(?:\/|$)/)[1] # get origin

        if origin.nil? || origin.strip.empty?
          errors << "#{filename[7..-1]}: #{attribute.parent.to_html}"
          next
        end

        unless
          [
            'fiveyellowmice.com',
            'util.fiveyellowmice.com',
            'ajax.aspnetcdn.com',
            'imgproxy.fym.one',
            'che.fym.moe',
            'cdn.rawgit.com',
            'matomo.fiveyellowmice.com',
            'pagead2.googlesyndication.com',
          ].include?(origin) ||
          origin =~ /^(?:localhost|127.0.0.1|\:\:1)(?:\:\d+)?$/
        then
          errors << "#{filename[7..-1]}: #{attribute.parent.to_html}"
        end

      end
    end

    unless errors.empty?
      raise "Found #{errors.length} unapproved origin#{errors.length == 1 ? '' : 's'}:\n  " + errors.join("\n  ")
    end

  end

end
