SUMMARY_GENERATOR = Proc.new do |content, length = nil, ellipsis = nil|
  tree = Nokogiri::HTML.fragment(content.render(layout: false))
  tree.css('.lead').text.strip
end
