module Middleman
  class BlogExtension
    option :locale, nil, "Locale pages generated for this blog are set to."
  end
end
  

class I18nAwareBlogPostIndices < Middleman::Extension
  
  def manipulate_resource_list(resources)
    resources.each do |resource|
      if resource.locals['blog_controller']
        resource.add_metadata(options: {locale: resource.locals['blog_controller'].options.locale})
      end
    end
  end

end

Middleman::Extensions.register :i18n_aware_blog_post_indices, I18nAwareBlogPostIndices
