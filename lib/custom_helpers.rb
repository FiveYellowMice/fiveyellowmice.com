require 'openssl'

class CustomHelpers < Middleman::Extension

  helpers do
    def encode_uri_component(s)
      URI.encode_www_form_component(s).gsub('+', '%20')
    end

    def baseurl
      if config[:http_prefix] == '/'
        ''
      else
        config[:http_prefix]
      end
    end

    def ietf_language_tag(lang)
      case lang
      when :zh
        'zh-CN'
      else
        lang.to_s
      end
    end

    def excerpt_for(page)
      page.data.excerpt || (page.respond_to?(:summary) && page.summary) || nil
    end

    def all_articles_all_languages
      locales.map{|l| blog("blog.#{l}").articles }.flatten(1).sort_by(&:date).reverse
    end

    def all_articles_prefer_language(lang = nil)
      lang = locale if lang.nil?

      articles = blog("blog.#{lang}").articles
      articles_en = blog("blog.en").articles
      articles_other = locales.select{|l| l != lang && l != :en }.map{|l| blog("blog.#{l}").articles }.flatten(1)
      #puts "Current lang has #{articles.length}, en has #{articles_en.length}, other has #{articles_other.length}."

      slug_for = Proc.new {|a| a.url.split('/').last }
      add_to_article_if_same_article_in_different_language_not_found = Proc.new do |a|
        a_slug = slug_for.call(a)
        unless articles.find {|b| slug_for.call(b) == a_slug }
          articles << a
        end
      end
      articles_en.each &add_to_article_if_same_article_in_different_language_not_found
      articles_other.each &add_to_article_if_same_article_in_different_language_not_found

      articles.sort_by(&:date).reverse
    end

    def alternative_languages_for(page)
      return [] unless page.options[:locale]
      results = []
      (locales - [page.options[:locale]]).each do |l|
        sitemap.resources.select do |res|
          if res.options[:locale] == l && res.destination_path[extensions[:i18n].path_root(l).length-1..-1] == page.destination_path[extensions[:i18n].path_root(page.options[:locale]).length-1..-1]
            results << res
            break
          end
        end
      end
      results
    end

    def postprocess_post(content, postprocessors, locale: nil)
      tree = Nokogiri::HTML.fragment(content)

      postprocessors.each do |postprocessor|
        case postprocessor

        when :add_article_end_mark
          last_paragraph = tree.css("p:root, .doc-section>p, .open-block>.content>p").last
          last_paragraph << "<span class=\"article-end-mark\" aria-hidden=\"true\"> \u{1F401}</span>"

        when :add_spoiler_noscript_notice
          alert = tree.css('.spoiler-alert')[0]
          if alert
            alert << '<noscript><aside style="color: #9d0000">' + t(:spoiler_noscript_notice_html, locale: locale) + '</aside></noscript>'
          end

        when :remove_hidden_spoilers
          tree.css('.spoiler:not(.spoiler-shown)').remove

        when :replace_local_urls
          %w(src href).each do |point|
            tree.css("*[#{point}]").each do |el|
              if el[point].start_with?('/') && !el[point].start_with?('//')
                el[point] = config[:site_origin] + el[point]
              end
            end
          end

        else
          raise "Unknown postprocessor #{postprocessor}"
        end

      end
      tree.to_html
    end

    def url_proxy(url)
      sig = OpenSSL::HMAC.hexdigest 'sha256', config[:url_proxy_key], url
      url_encoded = URI.encode_www_form_component(url).gsub('+', '%20')
      "#{config[:url_proxy_base]}/proxy?url=#{url_encoded}&sig=#{sig}"
    end

  end
end

Middleman::Extensions.register :custom_helpers, CustomHelpers
