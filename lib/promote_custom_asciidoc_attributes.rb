class PromoteCustomAsciiDocAttributes < Asciidoctor::Extensions::TreeProcessor
  @@promoted_attributes = []

  def self.promoted_attributes=(v)
    @@promoted_attributes = v
  end
  
  def process(document)
    @@promoted_attributes.each do |attribute|
      if document.attr? attribute
        document.set_attr("page-#{attribute.gsub('-', '_')}", document.attr(attribute))
      end
    end
  end
end

Asciidoctor::Extensions.register do
  treeprocessor PromoteCustomAsciiDocAttributes
end
