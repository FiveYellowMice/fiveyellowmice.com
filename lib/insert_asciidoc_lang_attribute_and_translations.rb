class InsertAsciiDocLangAttributeAndTranslations < Middleman::Extension

  def manipulate_resource_list(resources)
    resources.each do |resource|
      if resource.options[:renderer_options] && resource.options[:locale]
        resource.add_metadata options: {
          renderer_options: {
            attributes: {
              'lang' => resource.options[:locale].to_s
            }.merge(I18n.t(:asciidoc, locale: resource.options[:locale], default: {}).map{|k, v| [k.to_s, v] }.to_h)
          }
        }
      end
    end
  end

end

Middleman::Extensions.register :insert_asciidoc_lang_attribute_and_translations, InsertAsciiDocLangAttributeAndTranslations
