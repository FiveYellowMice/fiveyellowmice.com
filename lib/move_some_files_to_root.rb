class MoveSomeFilesToRoot < Middleman::Extension
  option :directories, [], "List of directories to move their files to root."

  self.resource_list_manipulator_priority = 101

  def manipulate_resource_list(resources)
    resources.each do |resource|
      if !resource.ignored? && options.directories.any? {|dir| resource.path.start_with?(dir + '/') }
        resource.destination_path = resource.destination_path.split('/')[1..-1].join('/')
      end
    end
  end
end

Middleman::Extensions.register :move_some_files_to_root, MoveSomeFilesToRoot
