module Middleman
  module AsciiDoc
    class AsciiDocExtension
      def merge_attributes attrs, initial = {}
        if (is_array = ::Array === attrs) || ::Hash === attrs
          attrs.each_with_object(initial) {|entry, new_attrs|
            key, val = is_array ? (((entry.split '=', 2) + ['', '']).slice 0, 2) : entry
            if key.start_with? '!'
              new_attrs[key.slice 1, key.length] = nil
            elsif key.end_with? '!'
              new_attrs[key.chop] = nil
            # "-" prefix means to remove key from accumulator
            elsif key.start_with? '-'
              new_attrs.delete (key.slice 1, key.length)
            elsif is_array
              new_attrs[key] = resolve_attribute_refs val, new_attrs
            else
              new_attrs[key] = case val
              when ::String
                resolve_attribute_refs val, new_attrs
              when ::Numeric
                val.to_s
              when true
                ''
              #when nil, false
              #  nil
              else
                val
              end
            end
          }
        else
          raise 'Value of attributes setting on AsciiDoc extension must be a Hash or an Array.'
        end
      end
    end
  end
end
