=begin

This pluigin allows the use of a .spoiler CSS class of block elements and span, which will be hidden by default on the webpage. Giving it a .spoiler-shown class will make it shown by default.

Spoilers may also be labeled with "tags", by giving .spoiler-for-{tag} classes to the same element. Then they will only be shown if the user chooses to show spoilers for all of the spoiler tags on the element.

Example: [.spoiler.spoiler-for-a-major.spoiler-for-b]

To allow the user to show or hide spoilers, a spoiler-button:[] inline macro is used. The "target" of the macro contains a comma-separated list of spoiler tags to operate on. It requires exactly 3 attributes, respectively: "show" or "hide", text to display on the button, text to display after the button is clicked and disabled.

Example: spoiler-button:a-major,a-minor[show, Click here to show major spoilers for A, Major spoilers for A are shown]

After clicking a "show" button, tags specified on the button will be considered shown, then, spoilers with no tags or have all tags considered shown will be shown. After clicking a "hide" button, all spoilers containing no tags or at least one of the tags specified on the button will be hidden. Clicking either type of button will disable the button until refresh.

Works in conjunction with source/js/spoilers.js .

A [SPOILER-ALERT] block style may be used in the document to alert the reader. It renders as an admonition block. The spoilers buttons should be put inside this block.

If JavaScript is disabled, all spoilers will be shown, and spoiler button will be hidden. So REMEMBER to put text stating the spoiler being hidden inside .noscript-hidden . A warning stating "to hide spoilers, enable JavaScript" will also be displayed at the bottom of the first [SPOILER-ALERT].

In RSS readers, spoilers not shown by default will be removed. Spoiler buttons and .noscript-hide elements still remain. So REMEMBER to set dynamic-content attribute to true on articles making use of this plugin, so RSS readers will see a notice telling them to open the webpage.

Why do I make it such complex?

=end

module AsciiDocHideSpoilers
  class SpoilerAlert < Asciidoctor::Extensions::BlockProcessor
    use_dsl
    named :'SPOILER-ALERT'
    contexts :paragraph, :example
    content_model :compound

    def process(parent, reader, attrs)
      new_attrs = {'name' => 'spoiler-alert', 'textlabel' => parent.document.attr('spoiler-alert-caption')}
      block = create_block parent, :admonition, reader.lines, attrs.merge(new_attrs), content_model: attrs['cloaked-context'] == :paragraph ? :simple : :compound
      block
    end
  end

  class SpoilerButton < Asciidoctor::Extensions::InlineMacroProcessor
    use_dsl
    named :'spoiler-button'
    name_positional_attributes 'action', 'button-label', 'activated-label'

    def process(parent, target, attrs)
      button = Nokogiri::XML::Node.new('button', Nokogiri::HTML::Document.new)
      button.content = attrs['button-label']
      button['data-spoiler-for'] = target
      button['data-activated-label'] = attrs['activated-label']
      button['data-action'] = attrs['action']
      button.add_class 'spoiler-button'
      button.to_html
    end
  end
end

Asciidoctor::Extensions.register do
  if document.basebackend? 'html'
    block AsciiDocHideSpoilers::SpoilerAlert
    inline_macro AsciiDocHideSpoilers::SpoilerButton
  end
end
