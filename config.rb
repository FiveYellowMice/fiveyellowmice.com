require 'dotenv'
Dotenv.load

Dir['lib/*.rb'].each do |lib|
  require lib
end


config[:site_origin] = 'https://fiveyellowmice.com' # do not include http_prefix
config[:author_email] = 'hkz85825915@gmail.com'
config[:site_time] = Time.now.utc

config[:time_zone] = 'UTC'
config[:css_dir] = 'css'

config[:url_proxy_base] = 'https://imgproxy.fym.one'
config[:url_proxy_key] = ENV['URL_PROXY_KEY'] || 'example'

page '/posts/*/*', data: {comments: true, in_sitemap: true}
page '/*.html', data: {cc_by_sa: true}

%w(index.html feed.xml seo/sitemap.xml).each do |p|
  page '/index.html', locale: :''
end


page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

ignore '/scss/*'

ignore '*.md'

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

locales = [:zh, :en]

activate :i18n do |i18n|
  i18n.locales = locales
  i18n.mount_at_root = false
end

PromoteCustomAsciiDocAttributes.promoted_attributes = %w(tags shortlink in-sitemap comments dynamic-content excerpt headpic headpic-alt headpic-caption attribution forbidden-to-chinese-viewers)

activate :asciidoc do |asciidoc|
  asciidoc.backend = :html5s
  asciidoc.attributes = {
    'site-origin' => config[:site_origin],
    'baseurl' => config[:http_prefix] == '/' ? '' : config[:http_prefix],
    'site-url' => config[:site_origin] + (config[:http_prefix] == '/' ? '' : config[:http_prefix]),
    'figure-caption' => false,
  }
end

locales.each do |lang|
  activate :blog do |blog|
    blog.name = "blog.#{lang}"
    blog.locale = lang
    blog.preserve_locale = true

    blog.sources = "#{lang}/posts/{year}/{year}-{month}-{day}-{title}.html"
    blog.permalink = "#{lang}/posts/{year}/{month}/{title}.html"
    blog.layout = "post"
    blog.default_extension = ".adoc"
    blog.summary_separator = ''
    blog.summary_generator = SUMMARY_GENERATOR

    blog.tag_template = "localizable/tags/tag.html"
    blog.taglink = "#{lang}/tags/{tag}.html"

    blog.generate_year_pages = false
    blog.generate_month_pages = false
    blog.generate_day_pages = false
  end
end

activate :i18n_aware_blog_post_indices

activate :insert_asciidoc_lang_attribute_and_translations

configure :build do
  activate :asset_hash, ignore: ['favicons/*', 'images/*']
end

activate :external_pipeline do |pipeline|
  pipeline.name = 'gulp'
  pipeline.command = build? ? 'npx gulp' : 'npx gulp watch'
  pipeline.source = 'gulp-dest'
  pipeline.latency = 1
end

activate :move_some_files_to_root, directories: ['favicons', 'seo']

activate :custom_helpers

configure :development do
  activate :livereload, livereload_css_target: nil
end
