#!/bin/bash

echo "Start deplying..."

chmod 600 $SSH_KEY

rsync \
  -re "ssh -o StrictHostKeyChecking=no -o LogLevel=ERROR -i $SSH_KEY" \
  --delete-after public/ \
  git@$DEPLOY_SERVER:/srv/www
